
// OB-запись имеет структуру [price, amount, total]
// Стоит отметить, что обращение по числовым индексам не самый красивый способ, но самый быстрый

const initialState = {
    bids: [],
    asks: []
};

const orderBook = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_ORDER_BOOK':
            return {
                asks: calculateTotals(action.asks, 0),
                bids: calculateTotals(action.bids, 0)
            };
        case 'UPDATE_RECORD':
            // новая запись для OB имеет вид [price, amount, side]
            return updateOrderBook(state, action.record);
        default:
            return state;
    }
};

export default orderBook;

// функция принимает весь массив записей для ask и bid одновременно
// и определяет, какой массив нуждается в обновлении
const updateOrderBook = ({asks, bids}, record) => ({
    asks: !!record[2] ? replaceRecord(asks, record) : asks,
    bids: !!record[2] ? bids : replaceRecord(bids, record)
});

// функция принимает весь массив записей только для ask или bid
const replaceRecord = (orders, record) => {
    let i = orders.findIndex(order => order[0] === record[0]);
    if (i !== -1) {
        orders.splice(i, 1, record);
    }
    return calculateTotals(orders, i);
};

// функция принимает весь массив записей только для ask или bid
// и суммирует тотал начиная с полученного индекса
// Стоит отметить, что цикл for не самый красивый способ перебора, но самый быстрый
const calculateTotals = (orders, changedIndex) => {
    let sum = changedIndex > 0 ? orders[changedIndex - 1][2] : 0;
    for (let i = changedIndex < 0 ? 0 : changedIndex, len = orders.length; i < len; i++) {
        orders[i][2] = sum += orders[i][1];
    }
    return [...orders];
};