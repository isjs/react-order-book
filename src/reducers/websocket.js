
const websocket = (state, action) => {
    switch (action.type) {
        case 'WS_OPENED':
            console.log('WebSocket was opened');
            return state;
        case 'WS_CLOSED':
            console.log('WebSocket was opened. Reason: ', action.reason);
            return state;
        default:
            return state;
    }
};

export default websocket;
