import query from './_query'

export default {
    getOrderBook() {
        return query({
            method: 'get',
            url: 'http://isjs.ru/api/utilities/orderBookExample',
            crossDomain: true
        });
    }
}