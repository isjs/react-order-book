import React, {PureComponent} from "react";

class OrderBookBar extends PureComponent {
    render() {
        return (
            <div className="ob-table__bar" style={{
                width: Math.round(this.props.total * 100 / this.props.max) + '%'
            }}/>
        )
    }
}

export default OrderBookBar;