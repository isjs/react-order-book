import React from 'react'
import OrderBook from './OrderBook'
import '../style.scss'

const App = () => (
    <div>
        <h2>Order Book</h2>
        <OrderBook />
        <div className="fps-indicator"></div>
    </div>
);

export default App