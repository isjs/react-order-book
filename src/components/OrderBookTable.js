import React, {Component} from "react";
import OrderBookRecord from "./OrderBookRecord"
import OrderBookBar from "./OrderBookBar"
import _ from "lodash"

class OrderBookTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            needRender: false,
            timeToRender: 0
        };
        this.startDemo = this.startDemo.bind(this);
        this.startStress = this.startStress.bind(this);
        this.callToRender = _.throttle(this.letRender, this.state.timeToRender);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.needRender) {
            this.setState({needRender: false});
            return true;
        } else {
            if (nextProps.asks !== this.props.asks || nextProps.bids !== this.props.bids) {
                this.callToRender();
            }
            return false;
        }
    }

    letRender() {
        this.setState({needRender: true});
    }

    startDemo() {
        this.setState({timeToRender: 500})
        this.props.wsSend(200);
    }

    startStress() {
        this.setState({timeToRender: 100})
        this.props.wsSend(10);
    }

    render() {
        const {asks, bids} = this.props,
            maxTotal = Math.max(asks[19] ? asks[19][2] : 0, bids[19] ? bids[19][2] : 0),
            getArrayOfRecords = arr => {
                return arr.map((order, index) => (
                    <div className="ob-table__row" key={'w' + order[0]}>
                        <OrderBookBar total={order[2]} row={index} key={'s' + order[0]} max={maxTotal}/>
                        <OrderBookRecord price={order[0]} amount={order[1]} key={'r' + order[0]}/>
                    </div>
                ))
            },
            actionButtons = show => {
                return show ? (
                    <div className="actions">
                        <button onClick={this.startDemo}>Demo: 5msg 2renders per second</button>
                        <button onClick={this.startStress}>Stress: 100msg 10renders per second</button>
                    </div>
                ) : (
                    <p>Reload the page to start another test</p>
                );
            };
        return (
            <div>
                <div className="ob-table">
                    <div className="ob-table__column ob-table__column--left">
                        {getArrayOfRecords(bids)}
                    </div>
                    <div className="ob-table__column ob-table__column--right">
                        {getArrayOfRecords(asks)}
                    </div>
                </div>
                {actionButtons(!this.state.timeToRender)}
            </div>
        );
    }
}

export default OrderBookTable;