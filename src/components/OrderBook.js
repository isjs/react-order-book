import React, {Component} from "react";
import {connect} from "react-redux";
import {setOrderBook, wsConnect, wsSend} from "../actions";
import OrderBookTable from "./OrderBookTable"
import api from "../api/api";

const mapDispatchToProps = dispatch => {
    return {
        setOrderBook: fullOrderBook => dispatch(setOrderBook(fullOrderBook)),
        wsConnect: url => dispatch(wsConnect(url)),
        wsSend: msg => dispatch(wsSend(msg))
    };
};

const mapStateToProps = state => {
    return {
        bids: state.orderBook.bids,
        asks: state.orderBook.asks
    };
};

class OrderBook extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.wsConnect('ws://isjs.ru:8080');
        api.market.getOrderBook().then(response => {
            this.props.setOrderBook(response.data);
        });
    }

    render() {
        return (
            <div>
                <OrderBookTable asks={this.props.asks}
                                bids={this.props.bids}
                                wsSend={this.props.wsSend}/>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderBook);