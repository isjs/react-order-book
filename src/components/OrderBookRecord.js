import React, {PureComponent} from "react";

class OrderBookRecord extends PureComponent {
    render() {
        return (
            <p className="ob-table__record">
                <span>{this.props.price.toFixed(2)}</span>
                <span>{this.props.amount.toFixed(2)}</span>
            </p>
        )
    }
}

export default OrderBookRecord;