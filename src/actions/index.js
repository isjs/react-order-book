export const setOrderBook = ({asks, bids}) => ({
    type: 'SET_ORDER_BOOK',
    asks,
    bids
});

export const updateRecord = record => ({
    type: 'UPDATE_RECORD',
    record
});

export const wsConnect = url => ({
    type: 'WS_CONNECT',
    url
});

export const wsDisconnect = ()=> ({
    type: 'WS_DISCONNECT'
});

export const wsSend = msg => ({
    type: 'WS_SEND',
    msg
});

export const wsOpened = () => ({
    type: 'WS_OPENED'
});

export const wsClosed = reason => ({
    type: 'WS_CLOSED',
    reason
});