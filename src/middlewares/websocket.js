import {wsClosed, wsOpened, updateRecord} from "../actions";

let ws;

export const websocket = ({dispatch}) => next => action => {
    switch (action.type) {
        case 'WS_CONNECT':
            ws = new WebSocket(action.url);
            ws.onopen = () => dispatch(wsOpened());
            ws.onclose = event => dispatch(wsClosed(event));
            ws.onmessage = msg => dispatch(updateRecord(JSON.parse(msg.data)));
            break;
        case 'WS_SEND':
            console.log(action.msg);
            ws.send(action.msg);
            break;
        case 'WS_DISCONNECT':
            ws.close();
            break;
    }

    return next(action);
};